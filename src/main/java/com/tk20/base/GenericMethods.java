package com.tk20.base;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;


public class GenericMethods extends BaseClass{


	public GenericMethods() {
		super();
	}


	public static void explicitWait(long explicitWaitTime, WebElement element) {
		try {
			wait = new WebDriverWait(driver, explicitWaitTime);
			wait.until(ExpectedConditions.elementToBeClickable(element));
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	

	public static String filePath(String keyTODataFilePath) throws IOException {
		String testDataFilePath = prop.getProperty(keyTODataFilePath);
		return testDataFilePath;
	}

	public static Object[][] getTestData(String inputFilePath, int index)  
	{
		System.out.println("reading URLs from excel");
		try {
			File src = new File(inputFilePath);
			System.out.println(src);
			FileInputStream fis = new FileInputStream(src);
			wb = new XSSFWorkbook(fis);
			sheet = wb.getSheetAt(index);
		} catch (FileNotFoundException e) {
			System.out.println("OOPS !!! File not Found at the location");
		} catch(IOException ie) {
			ie.printStackTrace();
		}
		Object[][] data = new Object[sheet.getLastRowNum()][sheet.getRow(0).getLastCellNum()];
		for (int i = 0; i< sheet.getLastRowNum(); i++) {
			for(int k = 0; k< sheet.getRow(i).getLastCellNum(); k++) {
			data[i][k] = sheet.getRow(i+1).getCell(k).toString();
		}
		}
		return data;
	}



	public static void navigateUrl(String url) {
		try {
		System.out.println("Navigating to --> " + url );
		if(!url.isEmpty())
		driver.navigate().to(url);
		}catch(Exception e) {
			GenericMethods.captureScreen();
			e.printStackTrace();
		}
	}

	public static String getPageURL() throws Exception {
		return driver.getCurrentUrl();
		
	}


	/*public static void loginToUrl(WebElement element1, WebElement element2,String uname, String pword) throws IOException {
		element1.sendKeys(uname);
		element2.sendKeys(pword);
	}*/


	public static WebElement getElement(String xPath) {
		WebElement webElement =  driver.findElement(By.xpath(xPath));
		return webElement;
	}
	
	
	public static Boolean verifyElement(WebElement element, String artifactName) {
		String artifact =  element.getText();
		return (artifact.equals(artifactName));
	}
	
	public static void sendClick(WebElement element) {
		element.click();
	}

	public static void waitAndClick(WebElement element, long timeInSeconds) {
		wait = new WebDriverWait(driver, timeInSeconds);
		wait.until(ExpectedConditions.elementToBeClickable(element));
		element.click();
	}

	public static void sendKeys(WebElement element, String text)  {
		element.sendKeys(text);
	}

	public static void fileUploadAutoIT(WebElement element, String text)  {
		try {
			element.click();
			Runtime.getRuntime().exec(text);
		}catch(Exception e) {
			GenericMethods.captureScreen();
			e.printStackTrace();
		}
		
	}
	
	public static void selectName(WebElement element, String text) {
		element.sendKeys(text);
		try {
			Thread.sleep(2000);
			element.sendKeys(Keys.ENTER);
		}catch(InterruptedException ie) {
			System.out.println("Time Out!");
			ie.printStackTrace();
		}
	}

	public static void switchToIframe(int iframeIndex) {
		driver.switchTo().frame(iframeIndex);
	}

	public static void switchToParentFrame() {
		driver.switchTo().parentFrame();
	}

	public static void selectDropDown(WebElement element,  int selectPriorityByIndex) {
		Select select = new Select(element);
		select.selectByIndex(selectPriorityByIndex);
	}

	public static void tearDown() {
		//driver.close();
		driver.quit();
	}	

	public static void switchToNextTab() {
		ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
	}

	public static void captureScreen() {
		Date date = new Date();
		TakesScreenshot scrShot = (TakesScreenshot) driver;
		File srcFile = scrShot.getScreenshotAs(OutputType.FILE);
		String currentDir = System.getProperty("user.dir");
		System.out.println("directory --> " + currentDir);
		try {
			String datetoPrint =new SimpleDateFormat("dd-M-yyyy hh:mm:ss").format(date).toString();
			datetoPrint=datetoPrint.replace("-", "_");
			datetoPrint=datetoPrint.replace(" ", "_");
			datetoPrint=datetoPrint.replace(":", "_");
			currentDir=currentDir+"\\screenshot\\"+datetoPrint.replace(" ", "");
			//System.out.println(currentDir);
			FileUtils.copyFile(srcFile, new File(currentDir  + ".jpeg"));
		} catch (IOException e) {
			System.out.println("Screenshot can not be saved");
			e.printStackTrace();
		} 
	}

	public static void iframeLocator() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		Integer numberOfFrames = Integer.parseInt(js.executeScript("return window.length").toString());
		System.out.println("Number of iframes on the page are " + numberOfFrames);
	}



}
