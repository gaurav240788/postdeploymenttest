package com.tk20.pages;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.tk20.base.BaseClass;
import com.tk20.base.GenericMethods;


public class LoginPage extends BaseClass {
	

	@FindBy(name="user")						WebElement username;
	@FindBy(name="pass")						WebElement password;
	@FindBy(name="login2")					    WebElement loginbutton;
	
	public LoginPage() {
		PageFactory.initElements(driver, this);
	}

	public HomePage loginHome(String uname, String pword) {
		System.out.println("Attempting login");
		GenericMethods.sendKeys(username, uname);
		GenericMethods.sendKeys(password, pword);
		GenericMethods.sendClick(loginbutton);
		return new HomePage();
	}
	
	public ReportsPage loginReports(String uname, String pword) {
		System.out.println("Attempting login");
		GenericMethods.sendKeys(username, uname);
		GenericMethods.sendKeys(password, pword);
		GenericMethods.sendClick(loginbutton);
		return new ReportsPage();
}
	
	public ArtifactPage loginArtifacts(String uname, String pword) {
		GenericMethods.sendKeys(username, uname);
		GenericMethods.sendKeys(password, pword);
		GenericMethods.sendClick(loginbutton);
		return new ArtifactPage();
}
}