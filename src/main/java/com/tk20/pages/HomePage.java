package com.tk20.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.tk20.base.BaseClass;
import com.tk20.base.GenericMethods;

public class HomePage extends BaseClass {
	
	/*
	 * This class is not required to run at the time of testing
	  */
	
		@FindBy(xpath = "//a[@href = '#Home']")						    	WebElement home;
	    @FindBy(xpath = "//a[@href = '#Reports']")					    	WebElement reports;
	
	  //Initializing Page objects
	public HomePage() {
		PageFactory.initElements(driver, this);
	}
	
	
	public MessagePage clickMessagePage() {
		GenericMethods.sendClick(home);
		return new MessagePage();
	}
	
	public ReportsPage artifactPage() {
		GenericMethods.sendClick(reports);
		return new ReportsPage();
	}
}
