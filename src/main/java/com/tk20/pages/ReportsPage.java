package com.tk20.pages;

import java.util.ArrayList;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.tk20.base.BaseClass;
import com.tk20.base.GenericMethods;

public class ReportsPage extends BaseClass{
	
	@FindBy(xpath = "//a[@href='#Reports']") 																		WebElement reportsTab;
	@FindBy(xpath = "//input[@name = 'searchReports']")																WebElement searchBox;
	@FindBy(xpath = "//a[@class = 'breadcrumb-show']/span[contains(text(),'Advisement 031: List of Programs')]")	WebElement reportZeroThirtyOne;
	@FindBy(xpath = "//span[@role = 'button']")																		WebElement searchButton;
	@FindBy(xpath = "//div[@class='logo tk20']")																	WebElement tk20Logo;

	public ReportsPage() {
		PageFactory.initElements(driver,this);
	}

	public void clickOnReportsTab() {
		System.out.println("clicking reports");
		GenericMethods.waitAndClick(reportsTab, 3);
	}
	
	public void searchReport(String report) {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Searching Report");
		GenericMethods.selectName(searchBox, report);
	}

	public void clickOnReport() {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Clicking on report");
		GenericMethods.waitAndClick(reportZeroThirtyOne, 5);
	}
	
	public void verifyReport() {
		System.out.println("Switching tab");
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		GenericMethods.switchToNextTab();
		try {
			Thread.sleep(7000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		String pageLink;
		try {
			pageLink = GenericMethods.getPageURL();
			System.out.println(pageLink);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		GenericMethods.captureScreen();
	}
}
