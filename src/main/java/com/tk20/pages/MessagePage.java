package com.tk20.pages;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.tk20.base.BaseClass;
import com.tk20.base.GenericMethods;

public class MessagePage extends BaseClass {
	
	@FindBy(xpath = "//a[@href = '#Home']")						    	WebElement home; 
	@FindBy(xpath = "//a[@href='#HomeHomeMessages']")				    WebElement message;
    @FindBy(xpath = "//a[@href='#HomeHomeMessagesComposeMessage']")	    WebElement composeMessage;
	@FindBy(id = "token-input-recipients")						    	WebElement recipient; 
    @FindBy(xpath = "//input[@title='Enter Subject']")				    WebElement subject;
    @FindBy(xpath = "//select[@name='priority']")						WebElement priority;
    @FindBy(xpath = "//div[@id='tadiv']")	    						WebElement messageBox;
    @FindBy(xpath = "/html[1]/body[1]")							    	WebElement messageBoxIframe; 
    @FindBy(xpath = "//button[@name='Submit']")						    WebElement submit;
    @FindBy(xpath = "//a[@class='icon-user']")	    					WebElement userIcon;
    @FindBy(xpath = "//a[contains(text(),'Sign out')]")					WebElement signOut;
    
	
	public MessagePage() {
		PageFactory.initElements(driver, this);
	}
	
	public void clickOnMessageTab() {
		System.out.println("clicking on messageTab");
		GenericMethods.sendClick(home);
		GenericMethods.waitAndClick(message, 2);
		GenericMethods.sendClick(composeMessage);
		System.out.println("clicked on composeMessage");
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void enterRecipients(String recipientName) {
		System.out.println("Entering Recipeint as -->" + recipientName );
		GenericMethods.selectName(recipient, recipientName);
	}
	
	public void enterSubject(String typeSubject) {
		GenericMethods.sendKeys(subject, typeSubject);
		System.out.println("Subject Entered");
	}
	
	public void selectPriority(int setIndex) {
		GenericMethods.selectDropDown(priority,setIndex );
		System.out.println("priority Selected");
	}
	
	public void enterMessage(String messageText) {
		System.out.println("clicked on messageBox");
		GenericMethods.waitAndClick(messageBox, 5);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		GenericMethods.switchToIframe(4);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Inside Iframe and typing message");
		GenericMethods.sendKeys(messageBoxIframe, messageText);
		System.out.println("Message Typed");
		GenericMethods.switchToParentFrame();
		System.out.println("Switched to parent frame");
	}
	
	public void submit() {
		System.out.println("clickin on submit");
		GenericMethods.waitAndClick(submit, 2);
	}
	
	public void logOut() {
		System.out.println("Exting the server");
		GenericMethods.waitAndClick(userIcon,2);
		GenericMethods.waitAndClick(signOut,2);
		System.out.println("logged out");
	}
}