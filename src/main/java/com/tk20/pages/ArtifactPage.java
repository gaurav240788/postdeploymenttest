package com.tk20.pages;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.tk20.base.BaseClass;
import com.tk20.base.GenericMethods;

public class ArtifactPage extends BaseClass{

	@FindBy(xpath = "//a[@href='#Artifacts']") 									WebElement aritfactTab;
	@FindBy(xpath = "//a[@href='#ArtifactsArtifactsVideos']") 					WebElement videosTab;
	@FindBy(xpath = "//span[contains(text(),'Create New Video')]") 				WebElement createNewVideo; 
	@FindBy (xpath = "//input[@name='uploadFile']")								WebElement selectFile;
	@FindBy (name = "title")													WebElement videoTitle; 
	@FindBy(xpath = "//button[contains(text(),'Create')]")						WebElement create;
	@FindBy(xpath = "") 														WebElement deleteFile;


	public ArtifactPage() {
		PageFactory.initElements(driver, this);
	}

	public void clickOnArtifactsTab(long timeInSeconds) {
		try {
			GenericMethods.waitAndClick(aritfactTab, timeInSeconds); 
			Thread.sleep(2000);
		}catch(Exception e) {
			GenericMethods.captureScreen();
			e.printStackTrace();
		}
	}

	public void clickOnVideosTab(long timeInSeconds) {
		try {
			GenericMethods.waitAndClick(videosTab, timeInSeconds);
		}catch(Exception e) {
			GenericMethods.captureScreen();
			e.printStackTrace();
		}
	}

	public void clickOnCreateNewVideo(long timeInSeconds) {
		try {
			GenericMethods.waitAndClick(createNewVideo, timeInSeconds );
		}catch(Exception e) {
			e.printStackTrace();
			GenericMethods.captureScreen();
		}
	}

	public void clickOnSelectFile(String pathToFile) {
		try {
			Thread.sleep(3000);
			GenericMethods.sendKeys(selectFile, pathToFile);
			/*GenericMethods.implicitWait(120, deleteFile);
			System.out.println("upload complete");*/
		}catch(Exception e) {
			GenericMethods.captureScreen();
			e.printStackTrace();
		}
	}

	public void setTitle(String vTitle) {
		try {
			GenericMethods.sendKeys(videoTitle, vTitle);
		}catch(Exception e) {
			GenericMethods.captureScreen();
			e.printStackTrace();
		}
	}

	public void clickOnCreate() {
		try {
			GenericMethods.sendClick(create);
		}catch(Exception e) {
			GenericMethods.captureScreen();
			e.printStackTrace();
		}
	}

	public void isArtifactPresent(String vTitle) {
		try{
			String tempPath = "//div[@class='table-wrap']//a[contains(text(),%s)]";
			tempPath = String.format(tempPath, "'"+vTitle+"'");
			System.out.println(tempPath);
			WebElement tempelement = GenericMethods.getElement(tempPath);
			System.out.println(tempelement);
			if(GenericMethods.verifyElement(tempelement, vTitle)) {
				System.out.println("Video Artifact Created");
			}
			else {
				System.out.println("Video Artifact not Created");
			}
		}catch(Exception e) {
			e.printStackTrace();
			GenericMethods.captureScreen();
		}
	}

	/*public WebElement getXPath() {
		String artifactTitle = prop.getProperty("vTitle");
		String videoArtifact = "//div[@class = 'max-width-text']/a[Contains(text(),%s]" ;
		String actualXpath = String.format(videoArtifact, "'"+artifactTitle+"'");
		return actualXpath;
	}*/


}
