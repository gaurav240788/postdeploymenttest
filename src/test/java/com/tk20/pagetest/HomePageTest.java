package com.tk20.pagetest;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.tk20.base.BaseClass;
import com.tk20.base.GenericMethods;
import com.tk20.pages.HomePage;
import com.tk20.pages.LoginPage;
import com.tk20.pages.MessagePage;

public class HomePageTest extends BaseClass {
	static LoginPage loginPage;
	static HomePage homePage;
	static MessagePage messagePage;
	
	public HomePageTest() {
		super();
	}
	
	@DataProvider(name = "serverName")
	public static Object[][] getData() throws IOException{
		Object[][] data = GenericMethods.getTestData(GenericMethods.filePath("pathToServerUrlFile"), 0);
		return data;
	}
	
	
	@BeforeMethod()
	public void setup() {
		initialization();
		loginPage = new LoginPage();
		messagePage = new MessagePage();
	}
	
	
	@Test(priority = 0, dataProvider = "serverName")
	public void verifyMesaageTab(String server) {
		String url = "https://" + server;
		System.out.println("Navigating to --> " + url );
		GenericMethods.navigateUrl(url);
		homePage = loginPage.loginHome(prop.getProperty("userName"),prop.getProperty("passWord"));
		messagePage = homePage.clickMessagePage();
	}
	
	
	@AfterMethod
	public void shutDownBrowser() {
		GenericMethods.tearDown();
	}
	
}
