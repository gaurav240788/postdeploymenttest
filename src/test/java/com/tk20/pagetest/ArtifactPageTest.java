package com.tk20.pagetest;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.Alert;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.tk20.base.BaseClass;
import com.tk20.base.GenericMethods;
import com.tk20.pages.ArtifactPage;
import com.tk20.pages.LoginPage;

public class ArtifactPageTest extends BaseClass {

	LoginPage loginPage;
	ArtifactPage artifactPage;

	public ArtifactPageTest() {
		super();
	}

	@BeforeMethod()
	public void setup() {
		initialization();
		loginPage = new LoginPage();
		artifactPage = new ArtifactPage();
	}

	@DataProvider(name = "serverName")
	public static Object[][] getData() throws IOException{
		Object[][] data = GenericMethods.getTestData(GenericMethods.filePath("pathToServerUrlFile"), 0);
		return data;
	}

	@Test(dataProvider = "serverName")
	public void createTestArtifact(String server){
		try {
			Date date = new Date();
			String url = "https://" + server;
			GenericMethods.navigateUrl(url);
			artifactPage = loginPage.loginArtifacts(prop.getProperty("userName"),prop.getProperty("passWord"));
			artifactPage.clickOnArtifactsTab(5);
			artifactPage.clickOnVideosTab(5);
			artifactPage.clickOnCreateNewVideo(4);
			artifactPage.clickOnSelectFile(prop.getProperty("pathToFile"));
			Thread.sleep(90000);//2min and 30sec
			String artifactTitle =new SimpleDateFormat("dd-M-yyyy hh:mm:ss").format(date).toString();
			artifactTitle=artifactTitle.replace("-", "_");
			artifactTitle=artifactTitle.replace(" ", "_");
			artifactTitle=artifactTitle.replace(":", "_");
			artifactTitle = prop.getProperty("videoTitle") + artifactTitle;
			artifactPage.setTitle(artifactTitle);
			artifactPage.setTitle(artifactTitle);
			artifactPage.clickOnCreate();
			Thread.sleep(1000);
			if((ExpectedConditions.alertIsPresent()) == null) {
				System.out.println("video uploaded and converted");
			}
			else {
				Alert alert = driver.switchTo().alert();
				alert.accept();
				Thread.sleep(60000);
				artifactPage.clickOnCreate();
			}
			artifactPage.isArtifactPresent(artifactTitle);
			
		}catch(Exception e) {
			GenericMethods.captureScreen();
			e.printStackTrace();
		}
	}
	
	
	/*@AfterMethod
	public void shutDownBrowser() {
		GenericMethods.tearDown();
	} */

}


