package com.tk20.pagetest;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.tk20.base.BaseClass;
import com.tk20.base.GenericMethods;
import com.tk20.pages.HomePage;
import com.tk20.pages.LoginPage;
import com.tk20.pages.MessagePage;
import com.tk20.pages.ReportsPage;

public class ReportsPageTest extends BaseClass{
	
	LoginPage loginPage;
	HomePage homePage;
	ReportsPage reportsPage;
	
	public ReportsPageTest() {
		super();
	}

	@BeforeMethod()
	public void setup() {
		initialization();
		loginPage = new LoginPage();
		reportsPage = new ReportsPage();
	}
	
	@DataProvider(name = "serverName")
	public static Object[][] getData() throws IOException{
		Object[][] data = GenericMethods.getTestData(GenericMethods.filePath("pathToServerUrlFile"), 0);
		return data;
	}

	@Test(dataProvider = "serverName")
	public void sendMessage(String server){
		String url = "https://" + server;
		GenericMethods.navigateUrl(url);
		reportsPage = loginPage.loginReports(prop.getProperty("userName"),prop.getProperty("passWord"));
		reportsPage.clickOnReportsTab();
		reportsPage.searchReport(prop.getProperty("reportName"));
		reportsPage.clickOnReport();
		reportsPage.verifyReport();
	}
	
	@AfterMethod
	public void shutDownBrowser() {
		GenericMethods.tearDown();
	} 
	
	
}
