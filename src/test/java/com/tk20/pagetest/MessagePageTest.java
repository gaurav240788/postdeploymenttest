package com.tk20.pagetest;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.tk20.base.BaseClass;
import com.tk20.base.GenericMethods;
import com.tk20.pages.HomePage;
import com.tk20.pages.LoginPage;
import com.tk20.pages.MessagePage;

public class MessagePageTest extends BaseClass{

	LoginPage loginPage;
	HomePage homePage;
	MessagePage messagePage;

	public MessagePageTest() {
		super();
	}

	@BeforeMethod()
	public void setup() {
		initialization();
		loginPage = new LoginPage();
		messagePage = new MessagePage();
	}

	@DataProvider(name = "serverName")
	public static Object[][] getData() throws IOException{
		Object[][] data = GenericMethods.getTestData(GenericMethods.filePath("pathToServerUrlFile"), 0);
		return data;
	}

	@Test(dataProvider = "serverName")
	public void sendMessage(String server){
		String url = "https://" + server;
		GenericMethods.navigateUrl(url);
		homePage = loginPage.loginHome(prop.getProperty("userName"),prop.getProperty("passWord"));
		messagePage.clickOnMessageTab();
		messagePage.enterRecipients(prop.getProperty("receiver"));
		messagePage.enterSubject(prop.getProperty("messageSubject"));
		messagePage.selectPriority(Integer.parseInt(prop.getProperty("messagePriority")));
		messagePage.enterMessage(prop.getProperty("messageToReceiver"));
		GenericMethods.captureScreen();
		messagePage.submit();
		messagePage.logOut();
	}

	@AfterMethod
	public void shutDownBrowser() {
		
		GenericMethods.tearDown();
		
	} 
}
