package com.tk20.pagetest;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.tk20.base.BaseClass;
import com.tk20.base.GenericMethods;
import com.tk20.pages.HomePage;
import com.tk20.pages.LoginPage;

public class LoginPageTest extends BaseClass {
		LoginPage loginPage;
		HomePage homePage;
	
	public LoginPageTest() {
		/* calling parents class constructor to read config.properties file.
		 *  otherwise it will throw NullPointerException at @BeforeMethod. */
		super();		
	}
	
	@DataProvider(name = "serverName")
	public static Object[][] getData() throws IOException{
		Object[][] data = GenericMethods.getTestData(GenericMethods.filePath("pathToServerUrlFile"), 0);
		return data;
	}
	
	
	@BeforeMethod
	public void setUpBrowser() {
		initialization();
		loginPage = new LoginPage();
	}
	
	@Test(dataProvider = "serverName")
	public void loginTest(String server) {
		String url = "https://" + server;
		System.out.println("Navigating to --> " + url );
		GenericMethods.navigateUrl(url);
		homePage = loginPage.loginHome(prop.getProperty("userName"),  prop.getProperty("passWord"));
	}
	
	@AfterMethod
	public void shutDownBrowser() {
		GenericMethods.tearDown();
	} 
}
